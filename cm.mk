# Boot animation
TARGET_SCREEN_HEIGHT := 1200
TARGET_SCREEN_WIDTH := 1920

# Inherit some common CyanogenMod stuff
$(call inherit-product, vendor/cm/config/common_full_tablet_wifionly.mk)

# Inherit device configuration
$(call inherit-product, device/rockchip/m8hd/full_m8hd.mk)

# Device identifier. This must come after all inclusions
PRODUCT_NAME := cm_m8hd
# Set product name
PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME=pipo_m8hd
